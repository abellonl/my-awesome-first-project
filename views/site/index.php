<?php
    use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>

<div class="site-index">
    
    <div class="jumbotron text-center bg-transparent"> 
        <h1 class="display-4">Consultas de Selección</h1>
    </div>
    
    <div class="body-content">
        <!-- Inicio de la primera fila -->
        <div class="row">
            <!--
            botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos) </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('DAO', ['site/consultal'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- 
            fin de consulta 1
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos) </p>
                        <p>
                            <?= Html::a('Active Record', ['site/consultalar'], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('DAO', ['site/consultal'], ['class' => 'btn btn-warning']) ?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- 
            fin de consulta 2
            -->
        </div>
    </div>
    
</div>
